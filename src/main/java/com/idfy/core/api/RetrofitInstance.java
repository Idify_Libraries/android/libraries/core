package com.idfy.core.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.idfy.core.BuildConfig;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = BuildConfig.BASE_URL;
    private static HashSet<String> cookies   = new HashSet<>();
    private static Context context;
    private static OkHttpClient okHttpClient  = new OkHttpClient.Builder()
            .addInterceptor(new AddCookiesInterceptor())
            .addInterceptor(new ReceivedCookiesInterceptor())
            .build();

    /**
     * Create an instance of Retrofit object
     * */


    public static Retrofit getRetrofitInstance(Context c) {
        if (retrofit == null) {
            context = c;
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                   // .addCallAdapterFactory (RxJavaCallAdapterFactory.create ())
                    .client (okHttpClient)
                    .build();
        }
        return retrofit;
    }

    public static class ReceivedCookiesInterceptor implements Interceptor {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            okhttp3.Response originalResponse = chain.proceed(chain.request());

            if (!originalResponse.headers("Set-Cookie").isEmpty()) {
                for (String header : originalResponse.headers("Set-Cookie")) {
                    cookies.add(header);
                }
                // Save the file named cookieData sharepreference
                SharedPreferences sharedPreferences = context.getSharedPreferences("cookieData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putStringSet("cookie", cookies);
                editor.commit();

            }
            return originalResponse;
        }
    }
    public static class AddCookiesInterceptor implements Interceptor {

        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request.Builder builder = chain.request().newBuilder();
            HashSet<String> perferences = (HashSet) context.getSharedPreferences("cookieData", Context.MODE_PRIVATE).getStringSet("cookie", null);
            if (perferences != null) {
                for (String cookie : perferences) {
                    builder.addHeader("Cookie", cookie);

                }
            }
           // builder.header("Content-Type", "application/json");
            //builder.header("Accept", "application/json");


            return chain.proceed(builder.build());
        }
    }
}
