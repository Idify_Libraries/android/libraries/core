package com.idfy.core.api;

import org.json.JSONObject;

public interface ApiCallBack {
    void onSessionSuccess(JSONObject object);
    void intermediateCallBack();
    void onSessionFailure(String message);
   /* void onSessionCreatedSuccessful(JSONObject object);
    void onTokenReceived(String sessionToken);
    void onSessionError(String message);*/
}
