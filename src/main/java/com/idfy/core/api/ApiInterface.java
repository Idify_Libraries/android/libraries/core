package com.idfy.core.api;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    @PUT
    Call<Void> uploadScreenShot(@Url String url, @Body RequestBody image);


    @GET("backend/captures/status")
    Call<Object> getCaptureStatus(@Header("override_session") boolean override,@Query("t") String token);


    @GET("video-kyc-backend/token")
    Call<Object> getSessionToken(@Query("task_id") String requestId, @Query("t") String token);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("ms-connection-check/initiate_check")
    Call<JsonObject> initHealthCheck(@Query("t") String token, @Body JsonObject object);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("ms-connection-check/submit_connection_stats")
    Call<JsonObject> submitHealthCheck(@Query("t") String token, @Body JsonObject object);


    @POST("https://webhook.site/1adc0e3f-c10e-4848-ba0f-fbd63df90db2")
    Call<Object> logger(@Body JSONObject object);

    @POST("")
    Call<Object> logger(@Url String url, @Body Map<String, Object> object);

    @POST("")
    Call<Object> logger(@Url String url, @Query("t") String token, @Body JSONObject object);


}
