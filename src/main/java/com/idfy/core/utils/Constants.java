package com.idfy.core.utils;

public class Constants {
    public static final String Status_Capture_Pending = "capture_pending";
    public static final String Status_Recapture_Pending = "recapture_pending";
    public static final String Status_Review = "review";
    public static final String Status_Failed = "failed";
    public static final String Status_Initiated = "initiated";
    public static final String Status_In_Progress = "in_progress";
    public static final String Status_Review_Required = "review_required";
    public static final String Status_Rejected = "rejected";
    public static final String Status_Approved = "approved";
    public static final String Status_Cancelled = "cancelled";
    public static final String Status_Processed = "processed";
    public static final String Status_Capture_Expired = "capture_expired";
}
