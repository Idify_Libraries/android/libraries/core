package com.idfy.core.logger;

import java.util.Optional;

public interface LogDetails {

    String serviceCategory = "serviceCategory";
    String service = "service";
    String event_type = "event_type";
    String event_name = "event_name";
    String timestamp = "timestamp";
    String component = "component";
    String eventSource = "event_source";
    String logger_session_id = "logger_session_id";
    String exceptionName = "exceptionName";
    String exceptionDescription = "exceptionDescription";
    String referenceID = "referenceID";
    String referenceType = "referenceType";
    String log_version = "log_version";
    String app_vsn = "app_vsn";

    String liveMonitoring = "liveMonitoring"; // boolean
    String publish_to_dlk = "publish_to_dlk"; // boolean
}
