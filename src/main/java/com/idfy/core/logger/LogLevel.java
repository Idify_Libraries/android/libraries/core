package com.idfy.core.logger;

public interface LogLevel {

    String Info = "info";
    String Error = "error";
    String Warning = "warn";
}
