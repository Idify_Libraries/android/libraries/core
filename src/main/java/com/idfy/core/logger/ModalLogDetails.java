package com.idfy.core.logger;

import java.util.HashMap;
import java.util.Optional;
import java.util.OptionalInt;

public class ModalLogDetails {

    public String service_category = "";
    public String service = "";
    public String timestamp = "";
    public String event_type = "";
    public String event_name = "";
    public String component = "";
    public String event_source = "";
    public String logger_session_id = "";
    public String exceptionName = "";
    public String exceptionDescription = "";
    public String reference_id = "";
    public String reference_type = "";
    public String log_version = "";
    public String app_vsn = "";

    public boolean liveMonitoring = false;
    public boolean publish_to_dlk = false;
}
