package com.idfy.core.logger;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

public class LoggerWrapper {

    private boolean ENABLE_LOGGING = true;
    private static LoggerWrapper loggerWrapper = null;
    private HashMap<String, Logger> loggers = new HashMap<>();

    // set private to the constructor so that the instance of the class cannot be created from other classes
    private LoggerWrapper() {
    }

    public static LoggerWrapper getInstance() {
        if (loggerWrapper == null)
            loggerWrapper = new LoggerWrapper();

        return loggerWrapper;
    }

    public void createLogger(Context context, String key, String token, String loggerUrl) {

        if (loggers.containsKey(key)) {
            throw new Error(key + " Logger cannot be invoked multiple times!");
        } else {
            Logger loggerInstance = new Logger(context);
            loggerInstance.createInstance(token, loggerUrl);


            // set defaults
            ModalLogDetails defaultLog = new ModalLogDetails();
            defaultLog.service_category = "capture";
            defaultLog.app_vsn = "1.0";
            defaultLog.log_version = "V1";

            loggerInstance.setDefaults(defaultLog);

            // set default meta
            Map<String, Object> meta = new HashMap<>();
            meta.put("capture_token", token);
            loggerInstance.setMetaDefaults(meta);

            loggers.put(key, loggerInstance);

        }
    }

    public void log(String logLevel, ModalLogDetails logDetails, Map<String, Object> meta) {

        if (!loggers.containsKey("capture") || !ENABLE_LOGGING) return;
        loggers.get("capture").log(logLevel, logDetails, meta);
    }

    // add param: meta?: any
    public void logPageRender(long loggerStartTime, String component, Map<String, Object> meta) {

        loggers.get("capture").logPageRender(loggerStartTime, component, meta);
    }

    // set default can be used within the mentioned parameteters only, provide the key and the modal data to set
    // default value for the required key
    public void setDefault(String key, ModalLogDetails defaultLog) {
        loggers.get(key).setDefaults(defaultLog);
    }

    public void setMetaDefaults(String key, Map<String, Object> metaDefaults) {
        loggers.get(key).setMetaDefaults(metaDefaults);
    }

    public void clearLoggers() {
        loggers.clear();
        loggerWrapper = null;
    }

    public void removeLogger(String key) {
        loggers.remove(key);
    }

    public Logger getLoggers(String key) {
        return loggers.get(key);
    }
}
