package com.idfy.core.logger;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.idfy.core.DALCapture;
import com.idfy.core.api.ApiCallBack;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

public class Logger {

    Context context;
    String token;
    String loggerUrl = "https://capture.kyc.idfystaging.com/js-logger/publish";


    DALCapture dalCapture;
    private Map<String, Object> defaultLogDetails;
    private Map<String, Object> metaDefaults;

    int logSequenceCtr = 0;

    String logger_session_id = UUID.randomUUID().toString();

    Logger(Context context) {
        this.context = context;
        dalCapture = DALCapture.getInstance();
        defaultLogDetails = new HashMap<>();
        metaDefaults = new HashMap<>();
    }

    public void createInstance(String token, String loggerUrl) {
        this.token = token;
        this.loggerUrl = loggerUrl;
    }


    public void setDefaults(ModalLogDetails defaultLogDetails) {

        Field[] allFields = defaultLogDetails.getClass().getDeclaredFields();
        try {
            for (Field field : allFields) {
                Class<?> targetType = field.getType();
                Object value = field.get(defaultLogDetails);
                if (!value.equals("")) {
                    this.defaultLogDetails.put(field.getName(), value);
                }
            }
        } catch (Exception e) {
            System.out.println("--->" + e);
        }
    }

    public void setMetaDefaults(Map<String, Object> metaDefaults) {
        this.metaDefaults.putAll(metaDefaults);
    }


    public void log(String logLevel, ModalLogDetails logDetails, Map<String, Object> meta) {

        Map<String, Object> logs = new HashMap<>();
        Map<String, Object> metalogs = new HashMap<>(); // represented as details

        // check and add default logs
        logs.putAll(defaultLogDetails);
        logs.put("log_level", logLevel);
        logs.put("timestamp", getTimeStamp());
        logs.put("logger_session_id", logger_session_id);

        // check and add default meta
        if (metaDefaults.size() > 0)
            metalogs.putAll(metaDefaults);

        // check and add run time send meta
        if (meta.size() > 0)
            metalogs.putAll(meta);

        metalogs.put("logSequenceNo", ++logSequenceCtr);
        metalogs.put("retransmitted", false);


        String data = new GsonBuilder().create().toJson(logDetails);
        //String referenceId = dalCapture.getRequestId() != null ? dalCapture.getRequestId() : dalCapture.getCaptureId();

        try {
            JSONObject obj = new JSONObject(data);

            /*if (referenceId != null) {
                obj.put("reference_id", referenceId);
                obj.put("reference_type", dalCapture.getRequestId() != null ? "AV.TaskID" : "CaptureID");
            }*/


            //adding meta
            /*JsonObject objDetails = new JsonObject();
            objDetails.addProperty("capture_id", dalCapture.getCaptureId());-
            objDetails.addProperty("capture_token", token);-
            objDetails.addProperty("logSequenceNo", ++logSequenceCtr);-
            objDetails.addProperty("profile_status", dalCapture.getStatus());-
            objDetails.addProperty("retransmitted", false);
            objDetails.addProperty("session_id", dalCapture.getSessionId());
            obj.put("details", objDetails);*/

            // remove blank parameters and add the objects
            // need realign all the objects then remove blank paramerers
            Iterator<String> keys = obj.keys();
            while (keys.hasNext()) {
                String key = keys.next();

                // || key.equals("liveMonitoring") dont add if false
                if (!obj.get(key).equals("") || key.equals("event_name")) {
                    logs.put(key, obj.get(key));
                }
            }

            logs.put("details", metalogs);

            if (logs.get("liveMonitoring").equals(false)) {
                logs.remove("liveMonitoring");
            }


            System.out.println("SampleLog: " + logs.toString());

            /*dalCapture.log(context, obj, token, loggerUrl, new ApiCallBack() {
                @Override
                public void onSessionSuccess(JSONObject object) {
                }

                @Override
                public void intermediateCallBack() {
                }

                @Override
                public void onSessionFailure(String message) {
                }
            });*/

            dalCapture.logWebHook(context, logs, loggerUrl, new ApiCallBack() {
                @Override
                public void onSessionSuccess(JSONObject object) {
                }

                @Override
                public void intermediateCallBack() {
                }

                @Override
                public void onSessionFailure(String message) {
                }
            });
        } catch (Exception e) {
            System.out.println("logger: " + e);
        }
    }


    // add param: meta?: any
    public void logPageRender(long loggerStartTime, String component, Map<String, Object> meta) {

        // Pick Assisted Video task ID for all Assisted Video related screens
        // and Capture ID for all screens visited before Assisted Video.

        String referenceId = dalCapture.getRequestId() != null ? dalCapture.getRequestId() : dalCapture.getCaptureId();


        ModalLogDetails dataRenderTat = new ModalLogDetails();
        dataRenderTat.service_category = "Capture";
        dataRenderTat.service = "Render";
        dataRenderTat.event_type = "Render.TAT";
        dataRenderTat.event_name = dalCapture.getTatSince(loggerStartTime);
        dataRenderTat.component = component;
        dataRenderTat.event_source = "componentDidMount";
        dataRenderTat.logger_session_id = logger_session_id;
        dataRenderTat.reference_id = referenceId;
        dataRenderTat.reference_type = dalCapture.getRequestId() != null ? "AV.TaskID" : "CaptureID";

        log(LogLevel.Info, dataRenderTat, meta);


        ModalLogDetails dataPageRender = new ModalLogDetails();
        dataPageRender.service_category = "Capture";
        dataPageRender.service = "Render";
        dataPageRender.event_type = "RenderPage";
        dataPageRender.event_name = component;
        dataPageRender.component = "LoggerWrapper";
        dataPageRender.event_source = "logPageRender";
        dataPageRender.logger_session_id = logger_session_id;
        dataPageRender.reference_id = referenceId;
        dataPageRender.reference_type = dalCapture.getRequestId() != null ? "AV.TaskID" : "CaptureID";

        log(LogLevel.Info, dataPageRender, meta);

        System.out.println("===========> reqid null" + dalCapture.getRequestId());

    }

    public void logPageVisit(String pageName, String pageComponent, String source) {
        if (dalCapture.getSessionId() != null) {
            ModalLogDetails dataPageVisit = new ModalLogDetails();
            dataPageVisit.service_category = "Capture";
            dataPageVisit.service = "Routing";
            dataPageVisit.event_type = "Landed";
            dataPageVisit.event_name = pageName;
            dataPageVisit.component = pageComponent;
            dataPageVisit.log_version = "v1";
            dataPageVisit.publish_to_dlk = true;
            dataPageVisit.logger_session_id = logger_session_id;
            if (source != null && !source.trim().equals(""))
                dataPageVisit.event_source = source;
            else
                dataPageVisit.event_source = "componentDidMount";

            log(LogLevel.Info, dataPageVisit, new HashMap<>());
        }
    }

    /*public void log(String logLevel, ModalLogDetails logDetails) {

        String data = new GsonBuilder().create().toJson(logDetails);

        //JsonObject obj = new JsonParser().parse(data).getAsJsonObject();
        try {
            JSONObject obj = new JSONObject(data);
            obj.put("log_level", logLevel);
            obj.put("timestamp", getTimeStamp());

            // check and add default logs
            for (Map.Entry<String, Object> entry : defaultLogDetails.entrySet()) {

                // if log value is set, don not apply default value
                //if (!obj.has(entry.getKey())) {
                    obj.put(entry.getKey(), entry.getValue().toString());
                //}
            }

            JsonObject objDetails = new JsonObject();
            objDetails.addProperty("logSequenceNo", ++logSequenceCtr);
            objDetails.addProperty("retransmitted", false);
            objDetails.addProperty("capture_token", token);

            obj.put("details", objDetails);

           *//* JSONObject temObj = obj;

            // remove blank values
            Iterator<String> keys = obj.keys();
            while(keys.hasNext()) {
                String key = keys.next();
                if (obj.get(key).equals("")){
                    temObj.remove(key);
                }
            }*//*

            System.out.println("SampleLog: "+obj.toString());

            *//*dalCapture.log(context, obj, token, loggerUrl, new ApiCallBack() {
                @Override
                public void onSessionSuccess(JSONObject object) {
                }

                @Override
                public void intermediateCallBack() {
                }

                @Override
                public void onSessionFailure(String message) {
                }
            });*//*

            dalCapture.logWebHook(context, obj, loggerUrl, new ApiCallBack() {
                @Override
                public void onSessionSuccess(JSONObject object) {
                }

                @Override
                public void intermediateCallBack() {
                }

                @Override
                public void onSessionFailure(String message) {
                }
            });
        }catch (Exception e){
            System.out.println("logger: "+e);
        }
    }*/

    private String getTimeStamp() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+05:30'");
        df.setTimeZone(tz);
        return df.format(new Date());
    }


}
