package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Audio {

    @SerializedName("audioLevel")
    @Expose
    private Double audioLevel;
    @SerializedName("totalAudioEnergy")
    @Expose
    private Double totalAudioEnergy;
    @SerializedName("codec")
    @Expose
    private String codec;
    @SerializedName("bytes")
    @Expose
    private int bytes;
    @SerializedName("bandwidth")
    @Expose
    private Double bandwidth;
    @SerializedName("averageBandwidth")
    @Expose
    private Double averageBandwidth;
    @SerializedName("latency")
    @Expose
    private int latency;
    @SerializedName("packetsLost")
    @Expose
    private int packetsLost;
    @SerializedName("jitter")
    @Expose
    private Double jitter;
    @SerializedName("jitterBufferDelay")
    @Expose
    private Double jitterBufferDelay;

    public Double getAudioLevel() {
        return audioLevel;
    }

    public void setAudioLevel(Double audioLevel) {
        this.audioLevel = audioLevel;
    }

    public Double getTotalAudioEnergy() {
        return totalAudioEnergy;
    }

    public void setTotalAudioEnergy(Double totalAudioEnergy) {
        this.totalAudioEnergy = totalAudioEnergy;
    }

    public String getCodec() {
        return codec;
    }

    public void setCodec(String codec) {
        this.codec = codec;
    }

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    public Double getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Double bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Double getAverageBandwidth() {
        return averageBandwidth;
    }

    public void setAverageBandwidth(Double averageBandwidth) {
        this.averageBandwidth = averageBandwidth;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public int getPacketsLost() {
        return packetsLost;
    }

    public void setPacketsLost(int packetsLost) {
        this.packetsLost = packetsLost;
    }

    public Double getJitter() {
        return jitter;
    }

    public void setJitter(Double jitter) {
        this.jitter = jitter;
    }

    public Double getJitterBufferDelay() {
        return jitterBufferDelay;
    }

    public void setJitterBufferDelay(Double jitterBufferDelay) {
        this.jitterBufferDelay = jitterBufferDelay;
    }
}
