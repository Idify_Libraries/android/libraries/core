package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bandwidth {
    @SerializedName("averageBandwidth")
    @Expose
    private double averageBandwidth;
    @SerializedName("bandwidth")
    @Expose
    private double bandwidth;
    @SerializedName("availableBandwidth")
    @Expose
    private double availableBandwidth;

    public double getAverageBandwidth() {
        return averageBandwidth;
    }

    public void setAverageBandwidth(double averageBandwidth) {
        this.averageBandwidth = averageBandwidth;
    }

    public double getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
    }

    public double getAvailableBandwidth() {
        return availableBandwidth;
    }

    public void setAvailableBandwidth(double availableBandwidth) {
        this.availableBandwidth = availableBandwidth;
    }
}
