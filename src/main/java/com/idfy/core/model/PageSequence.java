package com.idfy.core.model;

import org.json.JSONArray;

public class PageSequence {
    String page;
    JSONArray jsonArrays;

    public PageSequence(String page, JSONArray jsonArrays) {
        this.page = page;
        this.jsonArrays = jsonArrays;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public JSONArray getJsonArrays() {
        return jsonArrays;
    }

    public void setJsonArrays(JSONArray jsonArrays) {
        this.jsonArrays = jsonArrays;
    }
}
