package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("isChrome")
    @Expose
    private boolean isChrome = false;
    @SerializedName("currentTimestamp")
    @Expose
    private Long currentTimestamp;
    @SerializedName("currentTimestampISO")
    @Expose
    private String currentTimestampISO;
    @SerializedName("connectionType")
    @Expose
    private ConnectionType connectionType;
    @SerializedName("bandwidth")
    @Expose
    private Bandwidth bandwidth;
    @SerializedName("track")
    @Expose
    private Track track;
    @SerializedName("audio")
    @Expose
    private Audio audio;
    @SerializedName("video")
    @Expose
    private Video video;
    @SerializedName("resolutions")
    @Expose
    private Resolutions resolutions;
    @SerializedName("streamId")
    @Expose
    private String streamId;
    @SerializedName("MsRoomID")
    @Expose
    private int msRoomID;
    @SerializedName("MSRoomReferenceID")
    @Expose
    private String mSRoomReferenceID;
    @SerializedName("source")
    @Expose
    private String source = "Capture";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isChrome() {
        return isChrome;
    }

    public void setChrome(boolean chrome) {
        isChrome = chrome;
    }

    public Long getCurrentTimestamp() {
        return currentTimestamp;
    }

    public void setCurrentTimestamp(Long currentTimestamp) {
        this.currentTimestamp = currentTimestamp;
    }

    public String getCurrentTimestampISO() {
        return currentTimestampISO;
    }

    public void setCurrentTimestampISO(String currentTimestampISO) {
        this.currentTimestampISO = currentTimestampISO;
    }

    public ConnectionType getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(ConnectionType connectionType) {
        this.connectionType = connectionType;
    }

    public Bandwidth getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Bandwidth bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Resolutions getResolutions() {
        return resolutions;
    }

    public void setResolutions(Resolutions resolutions) {
        this.resolutions = resolutions;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public int getMsRoomID() {
        return msRoomID;
    }

    public void setMsRoomID(int msRoomID) {
        this.msRoomID = msRoomID;
    }

    public String getmSRoomReferenceID() {
        return mSRoomReferenceID;
    }

    public void setmSRoomReferenceID(String mSRoomReferenceID) {
        this.mSRoomReferenceID = mSRoomReferenceID;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
