package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Track {

    @SerializedName("audio")
    @Expose
    private TrackAudio audio;
    @SerializedName("video")
    @Expose
    private TrackVideo video;

    public TrackAudio getAudio() {
        return audio;
    }

    public void setAudio(TrackAudio audio) {
        this.audio = audio;
    }

    public TrackVideo getVideo() {
        return video;
    }

    public void setVideo(TrackVideo video) {
        this.video = video;
    }
}
