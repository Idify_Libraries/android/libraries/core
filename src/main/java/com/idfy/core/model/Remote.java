package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Remote {
    @SerializedName("candidateType")
    @Expose
    private String candidateType;
    @SerializedName("transport")
    @Expose
    private String transport;
    @SerializedName("ipAddress")
    @Expose
    private String ipAddress;
    @SerializedName("portNumber")
    @Expose
    private String portNumber;

    public String getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(String candidateType) {
        this.candidateType = candidateType;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(String portNumber) {
        this.portNumber = portNumber;
    }
}
