package com.idfy.core.model;

public class CodecModel {
    String type;
    String id;
    String mimeType;

    public CodecModel(String type, String id, String mimeType) {
        this.type = type;
        this.id = id;
        this.mimeType = mimeType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
