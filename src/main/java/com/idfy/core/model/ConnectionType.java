package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConnectionType {
    @SerializedName("local")
    @Expose
    private Local local;
    @SerializedName("remote")
    @Expose
    private Remote remote;
    @SerializedName("rtt")
    @Expose
    private String rtt;
    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public Remote getRemote() {
        return remote;
    }

    public void setRemote(Remote remote) {
        this.remote = remote;
    }

    public String getRtt() {
        return rtt;
    }

    public void setRtt(String rtt) {
        this.rtt = rtt;
    }
}
