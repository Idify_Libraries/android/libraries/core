package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackAudio {
    @SerializedName("enabled")
    @Expose
    private boolean enabled;
    @SerializedName("muted")
    @Expose
    private boolean muted;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("readyState")
    @Expose
    private String readyState;


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isMuted() {
        return muted;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getReadyState() {
        return readyState;
    }

    public void setReadyState(String readyState) {
        this.readyState = readyState;
    }
}
