package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Resolutions {
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("bandwidthLimitedResolution")
    @Expose
    private String bandwidthLimitedResolution = "false";
    @SerializedName("cpuLimitedResolution")
    @Expose
    private String cpuLimitedResolution = "false";

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getBandwidthLimitedResolution() {
        return bandwidthLimitedResolution;
    }

    public void setBandwidthLimitedResolution(String bandwidthLimitedResolution) {
        this.bandwidthLimitedResolution = bandwidthLimitedResolution;
    }

    public String getCpuLimitedResolution() {
        return cpuLimitedResolution;
    }

    public void setCpuLimitedResolution(String cpuLimitedResolution) {
        this.cpuLimitedResolution = cpuLimitedResolution;
    }
}
