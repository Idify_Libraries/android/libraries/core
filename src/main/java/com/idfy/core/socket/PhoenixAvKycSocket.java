package com.idfy.core.socket;

import android.net.Uri;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.json.JSONException;
import org.json.JSONObject;
import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.ChannelEvent;
import org.phoenixframework.channels.Envelope;
import org.phoenixframework.channels.IMessageCallback;
import org.phoenixframework.channels.ISocketOpenCallback;
import org.phoenixframework.channels.ITimeoutCallback;
import org.phoenixframework.channels.Socket;

import java.io.IOException;

public class PhoenixAvKycSocket {
    Socket socket;
    Channel channel;

    //wss://capture.kyc.idfystaging.com/backend/socket/capture/websocket?
    // t=S_3Yv784zeIc
    // &capture_id=89c0e8e5-aeb6-415d-93dd-1e8e75f8402a
    // &session_token=9d1cb897677e4fd7973fd5725bb15ed2&vsn=2.0.0

    private String webSocketUrl = "wss://capture.kyc.idfystaging.com/backend/socket/capture/websocket";

    private String token;
    private String capture_id = "89c0e8e5-aeb6-415d-93dd-1e8e75f8402a";
    private String session_token = "";

    IDfyCallback callback;
    public PhoenixAvKycSocket(){

    }
    public PhoenixAvKycSocket(String token, String capture_id,String session_token, IDfyCallback callback) {
        this.token = token;
        this.capture_id = capture_id;
        this.session_token = session_token;
        this.callback = callback;
    }

    public void connectSocket() {
        try {

            webSocketUrl = "wss://capture.kyc.idfystaging.com/backend/socket/capture/websocket?" +
                    "t="+token +
                    "&capture_id=" +capture_id+
                    "&session_token="+session_token;
            Uri.Builder url = Uri.parse(webSocketUrl).buildUpon();
            //url.appendQueryParameter("participant_id", this.participantId);

            socket = new Socket(url.build().toString());
            Log.d("socket-url", url.toString());

            socket.onClose(() -> {
                Log.d("Socket", "Socket connection closed");
                try {
                    socket.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            socket.onError(reason -> {
                //handleTerminalError(reason);
                Log.d("Socket", "Socket connection error: " + reason);
            });
            socket.onOpen(new ISocketOpenCallback() {
                @Override
                public void onOpen() {
                    System.out.println("Socket is open");
                    channel = socket.chan("session:"+capture_id, new ObjectNode(JsonNodeFactory.instance));

                    try {
                        channel.join()
                                .receive("ok", new IMessageCallback() {
                                    @Override
                                    public void onMessage(Envelope envelope) {
                                        System.out.println("JOINED with " + envelope.toString());
                                        sendCapturePayload();
                                    }
                                })
                                .receive("error", new IMessageCallback() {
                                    @Override
                                    public void onMessage(Envelope envelope) {
                                        System.out.println("Error " + envelope.getReason() + " " + envelope.getPayload());
                                    }
                                });

                        channel.on("fetch_config", new IMessageCallback() {
                            @Override
                            public void onMessage(Envelope envelope) {

                                System.out.println("fetch_config " + envelope.getReason() + " " + envelope.getPayload());
                            }
                        });
                        channel.on("reconnecting", new IMessageCallback() {
                            @Override
                            public void onMessage(Envelope envelope) {
                                System.out.println("reconnecting " + envelope.getReason() + " " + envelope.getPayload());

                            }
                        });

                        channel.on("new:msg", new IMessageCallback() {
                            @Override
                            public void onMessage(Envelope envelope) {
                                System.out.println("NEW MESSAGE: " + envelope.toString());
                            }
                        });
                        channel.on("connected", new IMessageCallback() {
                            @Override
                            public void onMessage(Envelope envelope) {
                                System.out.println("connected: " + envelope.toString());
                            }
                        });

                        channel.on(ChannelEvent.CLOSE.getPhxEvent(), new IMessageCallback() {
                            @Override
                            public void onMessage(Envelope envelope) {
                                System.out.println("CLOSED: " + envelope.toString());
                            }
                        });

                        channel.on(ChannelEvent.ERROR.getPhxEvent(), new IMessageCallback() {
                            @Override
                            public void onMessage(Envelope envelope) {
                                System.out.println("ERROR: " + envelope.toString());
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            socket.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //{page: "capture", payload: []}
    private void sendCapturePayload() {

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();

        ObjectNode payloadObj = new ObjectNode(JsonNodeFactory.instance);
        payloadObj.put("page", "capture");
        payloadObj.put("payload", arrayNode);

        System.out.println("6,10: " + payloadObj);
        try {
            channel.push("session:fetch_config", payloadObj)
                    .receive("ok", new IMessageCallback() {
                        @Override
                        public void onMessage(final Envelope envelope) {
                            Log.i("TAG", "6,10 : " + envelope);
                            JSONObject jsonObject = null;
                            try {
                                String result = envelope.getPayload().get("response").toString();
                                jsonObject = new JSONObject(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String request_uid = envelope.getPayload().get("response").get("data").get("config").get("tasks").get("vkyc.assisted_vkyc").get("request_uid").asText();
                            Log.i("TAG", "request_uid: " + request_uid);
                            callback.iDfyCallBack("ASSISTED_KYC_ENVELOPE",jsonObject);
                        }
                    })
                    .timeout(new ITimeoutCallback() {
                        @Override
                        public void onTimeout() {
                            Log.w("TAG", "MESSAGE timed out");
                        }
                    });

        } catch (Exception e) {
            System.out.println("==>" + e);
        }
    }

}
