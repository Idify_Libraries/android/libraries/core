package com.idfy.core.socket;

import org.json.JSONObject;

public interface ODSocketCallBack {
    void onConnected(String roomId,String participantId);


    void setScreenShotUpload(String artifact, String link);

    void onDisconnect(String code);


    void onCompleted(String message);
}
