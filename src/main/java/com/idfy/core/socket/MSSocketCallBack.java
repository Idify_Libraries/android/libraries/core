package com.idfy.core.socket;

import org.json.JSONObject;

public interface MSSocketCallBack {

    void onConnected(String roomId,String participantId);

    void setRoomCall(JSONObject roomCall);

    void stopConnection();

    void onReconnecting();

    void onDisconnect(String code);

    void onSocketReconnecting();

    void onCompleted(String message);
}
