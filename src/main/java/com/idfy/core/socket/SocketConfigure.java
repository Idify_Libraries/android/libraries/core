package com.idfy.core.socket;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.idfy.core.logger.LogLevel;
import com.idfy.core.logger.Logger;
import com.idfy.core.logger.ModalLogDetails;

import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.ISocketOpenCallback;
import org.phoenixframework.channels.Socket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SocketConfigure {

    private String webSocketUrl = "";
    private Socket socket;
    private Channel channel;
    private SocketConfigCallBack callBack;
    private int errorCtr = 0;
    private boolean isSocketDisconnected = false;
    Logger pgLogger;

    public SocketConfigure(String webSocketUrl, Logger pgLogger, SocketConfigCallBack callBack) {
        this.webSocketUrl = webSocketUrl;
        this.pgLogger = pgLogger;
        this.callBack = callBack;

        ModalLogDetails defaults  = new ModalLogDetails();
        defaults.component = "CaptureSocket";
        this.pgLogger.setDefaults(defaults);

        Map<String, Object> meta = new HashMap<>();
        meta.put("endpoint", webSocketUrl);
        this.pgLogger.setMetaDefaults(meta);

    }

    public void setUpSocket() {
        try {
            socket = new Socket(webSocketUrl, 5000);
            socket.reconectOnFailure(true);
            socket.connect();

            ModalLogDetails data  = new ModalLogDetails();
            data.service = "SocketConnect";
            data.event_type = "Invoked";
            data.event_name = webSocketUrl;
            data.event_source = "setupSocket";
            pgLogger.log(LogLevel.Info, data, new HashMap<>());


            socket.onOpen(new ISocketOpenCallback() {
                @Override
                public void onOpen() {
                    System.out.println("Capture socket Open");
                    isSocketDisconnected = false;
                    callBack.onSocketOpen();

                    ModalLogDetails data  = new ModalLogDetails();
                    data.service = "SocketConnect";
                    data.event_type = "Success";
                    data.event_name = webSocketUrl;
                    data.event_source = "setupSocket";
                    pgLogger.log(LogLevel.Info, data, new HashMap<>());

                }});

            socket.onClose(() -> {
                System.out.println("Capture socket close");
                callBack.onSocketError("Socket Connection Closed");

                ModalLogDetails dataClose  = new ModalLogDetails();
                dataClose.service = "SocketConnect";
                dataClose.event_type = "Closed";
                dataClose.event_name = "";
                dataClose.event_source = "setupSocket";
                pgLogger.log(LogLevel.Info, dataClose, new HashMap<>());

            });

            socket.onError(reason -> {
                System.out.println("Capture socket Error "+reason);
                if (!isSocketDisconnected){
                    callBack.onSocketDisconnected();
                    isSocketDisconnected = true;
                }
                callBack.onSocketError(reason);

                ModalLogDetails dataError  = new ModalLogDetails();
                dataError.service = "SocketConnect";
                dataError.event_type = "OnError";
                dataError.event_name = reason;
                dataError.event_source = "setupSocket";

                pgLogger.log(LogLevel.Warning, dataError, new HashMap<>());
            });
            callBack.initSocket(socket);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setupChannel(Socket socket, String topic,ObjectNode payload){
        channel = socket.chan(topic, payload);
        if (channel != null) {
            callBack.initChannel(channel);
        }
    }

    private void disconnectSocket(){
        try {
            System.out.println("Disconnecting socket ");
            channel.leave();
            socket.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
