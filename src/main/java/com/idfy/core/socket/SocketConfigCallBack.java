package com.idfy.core.socket;

import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.Socket;

public interface SocketConfigCallBack {
    void initSocket(Socket socket);
    void initChannel(Channel channel);
    void onSocketError(String message);
    void onSocketDisconnected();
    void onSocketOpen();
}
