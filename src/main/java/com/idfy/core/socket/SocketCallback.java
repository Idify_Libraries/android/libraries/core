package com.idfy.core.socket;

import org.json.JSONObject;

public interface SocketCallback{
    void setRoomCall(JSONObject roomCall);

    void onReconnecting();


    void onDisconnect(String code);

    void onSocketReconnecting();



}
