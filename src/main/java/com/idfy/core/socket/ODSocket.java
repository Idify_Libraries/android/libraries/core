package com.idfy.core.socket;

import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.idfy.core.BuildConfig;
import com.idfy.core.DALCapture;

import org.json.JSONObject;
import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.ChannelEvent;
import org.phoenixframework.channels.Envelope;
import org.phoenixframework.channels.IMessageCallback;
import org.phoenixframework.channels.ISocketCloseCallback;
import org.phoenixframework.channels.ISocketOpenCallback;
import org.phoenixframework.channels.ITimeoutCallback;
import org.phoenixframework.channels.Socket;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ODSocket {
    Socket socket;
    Channel channel;

    String webSocket = "aj1CiPIceb_p";

    String session_id = "95fdd140e21a49e1970cf8f77ae73c8a"; // SessionId
    String public_id = "92078d08-3f71-4e5b-a174-8f6c37fa6968"; // task id, capture id
    String session_token = "57534181-22ab-4f4d-a3fc-26cf51978fa5";

    private String OD_URI = BuildConfig.OD_SOCKET_URL + "t=" + webSocket +
            "&public_id=" + public_id +
            "&session_token=" + session_token;

    ODSocketCallBack callback;
    MSSocket phoeRoomIdSocket;
    DALCapture dalCapture;

    public ODSocket(ODSocketCallBack callback) {
        this.callback = callback;
    }

    public ODSocket(ODSocketCallBack callback, String token, String session_id, String public_id, String session_token) {
        this.callback = callback;
        this.webSocket = token;
        this.session_id = session_id;
        this.public_id = public_id;
        this.session_token = session_token;
        dalCapture = DALCapture.getInstance();
        OD_URI = "wss://capture.kyc.idfystaging.com/video-kyc-backend/socket/task/websocket?t=" + webSocket +
                "&public_id=" + public_id +
                "&session_token=" + session_token;
    }

    public void connectSocket() {
        ObjectNode obj = null;
        try {

            obj = new ObjectNode(JsonNodeFactory.instance)
                    .put("user_agent", "Abhi")
                    .put("capture_session_id", session_id);


            socket = new Socket(OD_URI);
            socket.onClose(new ISocketCloseCallback() {
                @Override
                public void onClose() {
                    Log.d("OD Socket", "Socket connection closed");
                }
            });
            socket.onError(reason -> {
                //handleTerminalError(reason);
                Log.d("Socket", "OD Socket connection error: " + reason);
            });
            socket.onOpen(new ISocketOpenCallback() {
                @Override
                public void onOpen() {
                    System.out.println("OD Socket is open");

                }
            });
            socket.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        channel = socket.chan("session:" + public_id, obj);

        try {
            channel.join()
                    .receive("error", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {
                            System.out.println("Error " + envelope.getReason() + " " + envelope.getPayload());
                        }
                    })
                    .receive("ok", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {
                            System.out.println("JOINED with " + envelope.toString());
                        }
                    });

            channel.on("new:msg", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("NEW MESSAGE: " + envelope.toString());
                }
            });
            channel.on("fetch_task_data", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("fetch_task_data: " + envelope.toString());
                    sendTaskDataPayload();
                }
            });
            channel.on("waiting_status", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("waiting_status: " + envelope.toString());
                    Log.d("===Waiting", envelope.toString());
                }
            });
            channel.on("_disconnect", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    Log.d("===Waiting", envelope.toString());
                    System.out.println("waiting_status disconnect: " + envelope.toString());
                    String code = envelope.getPayload().get("code").asText();
                    callback.onDisconnect(code);
                }
            });
            channel.on("completed", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    callback.onCompleted(envelope.getPayload().get("status").asText());
                }
            });
            channel.on("connected", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("connected: " + envelope.toString());
                    Log.d("==Connected", envelope.toString());

                    String roomId = envelope.getPayload().get("room_sid").asText();
                    String participantId = envelope.getPayload().get("user_sid").asText();
                    callback.onConnected(roomId, participantId);

//                    phoeRoomIdSocket = new MSSocket(roomId, participantId, callback);
//                    phoeRoomIdSocket.connectSocket();

                }
            });

            channel.on("screenshot", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("screenshot event: " + envelope.toString());

                    String artifact_key = envelope.getPayload().get("artifact_key").asText();
                    String link = envelope.getPayload().get("link").asText();

                    System.out.println(artifact_key);
                    System.out.println(link);

                    link = link + "&t=" + webSocket + "%2F" + session_id + "&auth_service=pg&type=session";
                    System.out.println("new upload link: " + link);

                    sendScreenShotSuccess(artifact_key, "screenshot_initiated");
                    sendScreenShotSuccess(artifact_key, "screenshot_captured");
                    callback.setScreenShotUpload(artifact_key, link);
                    //String participantId = envelope.getPayload().get("user_sid").asText();

                }
            });

            channel.on(ChannelEvent.CLOSE.getPhxEvent(), new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("CLOSED: " + envelope.toString());
                }
            });

            channel.on(ChannelEvent.ERROR.getPhxEvent(), new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("ERROR: " + envelope.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    Executor executor = Executors.newSingleThreadExecutor();

    public void sendScreenShotSuccess(String artifactKey, String captureStatus) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonNode data = new ObjectNode(JsonNodeFactory.instance)
                            .put("event", artifactKey);

                    channel.push(captureStatus, data);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void disconnect() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (socket != null) {
                        // channel = null;
                        socket.disconnect();
                        // socket = null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    private void sendTaskDataPayload() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        JsonNode data = new ObjectNode(JsonNodeFactory.instance)
                .put("latitude", dalCapture.getLatitude())
                .put("longitude", dalCapture.getLongitude())
                .put("altitude", dalCapture.getAltitude())
                .put("accuracy", dalCapture.getAccuracy())
                .put("altitudeAccuracy", "")
                .put("heading", "")
                .put("speed", dalCapture.getSpeed())
                .put("timestamp", nowAsISO);

        ObjectNode payload = new ObjectNode(JsonNodeFactory.instance);
        payload.put("attr", "location");
        payload.put("data", data);


        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();
        arrayNode.addAll(Arrays.asList(payload));

        ObjectNode payloadObj = new ObjectNode(JsonNodeFactory.instance);
        payloadObj.put("payload", arrayNode);

        try {
            channel.push("task_data", payloadObj)
                    .receive("ok", new IMessageCallback() {
                        @Override
                        public void onMessage(final Envelope envelope) {
                            if (!envelope.getPayload().get("response").get("validations").isEmpty()) {
                                Log.i("TAG", "6,10 : TRUE");
                                callback.onDisconnect("LOCATION_MISMATCH");
                            }
                        }
                    })
                    .timeout(new ITimeoutCallback() {
                        @Override
                        public void onTimeout() {
                            Log.w("TAG", "MESSAGE timed out");
                        }
                    });
            System.out.println("==>" + payload);
        } catch (Exception e) {
            System.out.println("==>" + e);
            Log.i("==TAG", "MESSAGE: ");
        }

        /*
            "task_data",
            {
                "payload": [
                    {
                        "attr": "location",
                        "data": {
                            "latitude": 19.025340099999998,
                            "longitude": 73.0897722,
                            "altitude": null,
                            "accuracy": 77,
                            "altitudeAccuracy": null,
                            "heading": null,
                            "speed": null,
                            "timestamp": "2021-07-06T12:07:41.444Z"
                        }
                    }
                ]
            }*/
    }

    // {"ms_room_id":40850,"participant_id":"91b14b01-ba94-41de-803e-ca41f5a4d2da"}
    public void sendNoAudio_Video(String message) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ObjectNode payloadObj = new ObjectNode(JsonNodeFactory.instance);
                payloadObj.put("message", message);
                System.out.println("chat" + payloadObj);
                try {
                    channel.push("chat", payloadObj)
                            .receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    Log.i("TAG", "6,10 : " + envelope);
                                }
                            })
                            .timeout(new ITimeoutCallback() {
                                @Override
                                public void onTimeout() {
                                    Log.w("TAG", "MESSAGE timed out");
                                }
                            });
                } catch (Exception e) {
                    System.out.println("==>" + e);
                }

            }
        });
    }

    public void sendEndCallEvent(String code, String reason, boolean manual) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ObjectNode payloadObj = new ObjectNode(JsonNodeFactory.instance);
                payloadObj.put("code", code);
                payloadObj.put("reason", reason);
                payloadObj.put("manual", manual);
                System.out.println("chat" + payloadObj);
                try {
                    channel.push("end_call", payloadObj)
                            .receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    Log.i("TAG", "6,10 : " + envelope);
                                }
                            })
                            .timeout(new ITimeoutCallback() {
                                @Override
                                public void onTimeout() {
                                    Log.w("TAG", "MESSAGE timed out");
                                }
                            });
                } catch (Exception e) {
                    System.out.println("==>" + e);
                }

            }
        });
    }


    public void sendEvent(String event, JSONObject jsonObject) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonNode = mapper.readTree(jsonObject.toString());

                    channel.push(event, jsonNode)
                            .receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    Log.d("==DONE","DONE");

                                }
                            })
                            .timeout(new ITimeoutCallback() {
                                @Override
                                public void onTimeout() {
                                }
                            });

                } catch (Exception e) {
                    System.out.println("==>" + e);
                }
            }
        });
    }
}
