package com.idfy.core.socket;

import org.json.JSONObject;

public interface IDfyCallback {
    void iDfyCallBack(String event, JSONObject envelope);

}
