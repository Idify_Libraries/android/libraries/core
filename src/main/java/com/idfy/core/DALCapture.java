package com.idfy.core;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.idfy.core.api.ApiCallBack;
import com.idfy.core.api.ApiInterface;
import com.idfy.core.api.RetrofitInstance;
import com.idfy.core.logger.LogLevel;
import com.idfy.core.logger.Logger;
import com.idfy.core.logger.LoggerWrapper;
import com.idfy.core.logger.ModalLogDetails;
import com.idfy.core.model.PageSequence;
import com.idfy.core.model.ThemeConfig;
import com.idfy.core.socket.SocketConfigCallBack;
import com.idfy.core.socket.SocketConfigure;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.Envelope;
import org.phoenixframework.channels.IMessageCallback;
import org.phoenixframework.channels.ITimeoutCallback;
import org.phoenixframework.channels.Socket;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * DalCapture, the Data access layer for socket communication, api call and all core common functionalities
 * Create a instance of this Class in your Fragment by calling {@link #getInstance()}
 * and with the created instance call {@link #getCaptureStatusDetails}
 */
public class DALCapture {

    private SocketConfigure socketConfigure;
    private String token;
    private String captureId;
    private String sessionId;
    private String requestId;
    private Socket captureSocket;
    private Channel captureChannel;
    private ApiCallBack apiCallBack;
    private static DALCapture dalCapture = null;
    private List<PageSequence> listPageSeq = new ArrayList<>();
    private ThemeConfig themeConfig = null;
    private int pageIndex = 0;
    private String status;
    private String latitude;
    private String longitude;
    private String accuracy;
    private String altitude;
    private String speed;
    private boolean isVideoOverlayEnabled = false;
    private boolean isCallCompleted;
    Executor executor = Executors.newSingleThreadExecutor();
    public static final String SOCKET_DISCONNECT = "SOCKET_DISCONNECTED";
    private Logger pgLogger;

    /**
     * Default constructor
     */
    private DALCapture() {
    }

    /**
     * To get the instance of dal capture call this method
     * @return dalCapture :DalCapture Instance
     */
    public static DALCapture getInstance() {
        if (dalCapture == null)
            dalCapture = new DALCapture();

        return dalCapture;
    }

    /**
     * This will be 1st method that needs to be called.
     * This method calls the status api and provides you with the theme details, session Id, capture Id, token status
     * below params are required.
     * On Response you will receive theme configuration, headers, footers, colors by which you can set your theme configuration.
     * Next you will be required to call the {@link #getPageSequence}
     *
     * @param context Context
     * @param token Token
     * @param override Override the session (true / false)
     * @param apiCallBack {@link ApiCallBack} which provides the call back of api in
     * onSessionSuccess(), intermediateCallBack(), onSessionFailure().
     */
    public void getCaptureStatusDetails(Context context, String token, boolean override, ApiCallBack apiCallBack) {
        setToken(token);
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class).getCaptureStatus(override, token).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.e("==TAG", "response 33: " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    themeConfig = new ThemeConfig();
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        sessionId = jsonObject.getJSONObject("body").getString("session_id");
                        captureId = jsonObject.getJSONObject("body").getString("capture_id");
                        status = jsonObject.getJSONObject("body").getString("status");
                        JSONObject headerObject = jsonObject.getJSONObject("header");
                        String logo = headerObject.optString("logo");
                        themeConfig.setLogo(logo);
                        JSONObject themeObject = headerObject.getJSONObject("theme_config");
                        parseThemeConfig(themeObject);
                        apiCallBack.onSessionSuccess(headerObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        apiCallBack.onSessionFailure(jObjError.getString("message"));
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e("==TAG", "onFailure: " + t.getMessage());
                apiCallBack.onSessionFailure(t.getMessage());
            }
        });
    }

    /**
     * This will be 2nd method that needs to be called after getting the status details.
     * getPageData Connects to the socket connection and provides you with page sequences, validation configuration in callback.
     * On Success response, call {@link #getFirstPage()} which returns you with the {@link PageSequence}.
     * fetch the page from PageSequence and call {@link #fetchPageConfig}  method with
     * required parameters and payloads.
     *
     */
    public void getPageSequence(ApiCallBack apiCallBack) {
        String webSocketUrl = BuildConfig.SOCKET_URL +
                "t=" + token +
                "&capture_id=" + captureId +
                "&session_token=" + sessionId;

        pgLogger = LoggerWrapper.getInstance().getLoggers("pg");

        socketConfigure = new SocketConfigure(webSocketUrl, pgLogger, new SocketConfigCallBack() {
            @Override
            public void initSocket(Socket socket) {
                captureSocket = socket;

                ModalLogDetails data  = new ModalLogDetails();
                data.service = "ChannelJoin";
                data.event_type = "Invoked";
                data.event_name = "session:" + captureId;
                data.event_source = "channel";
                pgLogger.log(LogLevel.Info, data, new HashMap<>());

                socketConfigure.setupChannel(captureSocket, "session:" + captureId, new ObjectNode(JsonNodeFactory.instance));
            }

            @Override
            public void initChannel(Channel channel) {
                captureChannel = channel;
                if (captureChannel != null) {
                    joinChannel(apiCallBack);
                }
            }

            @Override
            public void onSocketError(String message) {
                apiCallBack.onSessionFailure(message);
            }

            @Override
            public void onSocketDisconnected() {
                apiCallBack.onSessionFailure(SOCKET_DISCONNECT);
            }

            @Override
            public void onSocketOpen() {
                apiCallBack.onSessionSuccess(null);
            }
        });
        socketConfigure.setUpSocket();

    }

    /**
     * Use this method to get the page related data. This method will be called, when you get a success response
     * from {@link #getPageSequence(ApiCallBack)}
     *
     * @param event Name of the event eg: "session:fetch_config"
     * @param jsonObject payload of the event eg: {page: "pageName", payload: []}
     * @param payApiCallBack ApiCallBack instance which provides the call back of fetPageConfig in
     * onSessionSuccess(), intermediateCallBack(), onSessionFailure().
     *
     */
    public void fetchPageConfig(String event, JSONObject jsonObject, ApiCallBack payApiCallBack) {

        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonNode = mapper.readTree(jsonObject.toString());

                    captureChannel.push(event, jsonNode)
                            .receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    Log.i("TAG", "6,10 : " + envelope);
                                    JSONObject jsonObject = null;
                                    try {
                                        String result = envelope.getPayload().get("response").toString();
                                        jsonObject = new JSONObject(result);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    payApiCallBack.onSessionSuccess(jsonObject);
                                }
                            })
                            .timeout(new ITimeoutCallback() {
                                @Override
                                public void onTimeout() {
                                    Log.w("TAG", "MESSAGE timed out");
                                }
                            });

                } catch (Exception e) {
                    System.out.println("==>" + e);
                }
            }
        });
    }

    public void sendEvent(String event,JSONObject jsonObject){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonNode = mapper.readTree(jsonObject.toString());

                    captureChannel.push(event, jsonNode)
                            .receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    Log.i("TAG", "6,10 : " + envelope);
                                }
                            })
                            .timeout(new ITimeoutCallback() {
                                @Override
                                public void onTimeout() {
                                    Log.w("TAG", "MESSAGE timed out");
                                }
                            });

                } catch (Exception e) {
                    System.out.println("==>" + e);
                }
            }
        });
    }
    
    /**
     * Use this method to disconnect the dalcapture socket connection
     */
    public void disconnectDalSocket() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (captureSocket != null) {
                    try {
                        captureChannel.leave();
                        captureSocket.disconnect();
                        captureChannel = null;
                        captureSocket = null;
                        listPageSeq.clear();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void parsePageSequence(JSONObject object){
        try {
            listPageSeq.clear();
            JSONArray pageSequence = object.getJSONArray("page_sequence");
            for (int i = 0; i < pageSequence.length(); i++) {
                JSONObject jsonObject = pageSequence.getJSONObject(i);
                String page = jsonObject.optString("page");
                JSONArray validations = jsonObject.getJSONArray("validations");
                listPageSeq.add(new PageSequence(page, validations));
            }
        }catch (Exception e){
        }
    }



    private void joinChannel(ApiCallBack apiCallBack) {
        try {
            captureChannel.join()
                    .receive("ok", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {

                            ModalLogDetails data  = new ModalLogDetails();
                            data.service = "ChannelJoin";
                            data.event_type = "Success";
                            data.event_name = captureChannel.getTopic();
                            data.event_source = "channel";
                            pgLogger.log(LogLevel.Info, data, new HashMap<>());

                            if (listPageSeq.size() == 0) {
                                JSONObject jsonObject = null;
                                try {
                                    String result = envelope.getPayload().get("response").toString();
                                    jsonObject = new JSONObject(result);
                                    parsePageSequence(jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                apiCallBack.onSessionSuccess(jsonObject);
                            }
                        }
                    })
                    .receive("error", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {
                            apiCallBack.onSessionFailure(envelope.getReason());

                            ModalLogDetails data  = new ModalLogDetails();
                            data.service = "ChannelJoin";
                            data.event_type = "Error";
                            data.exceptionName = envelope.getReason();
                            data.exceptionDescription = "Channel join error callback "+envelope.getReason();
                            pgLogger.log(LogLevel.Error, data, new HashMap<>());
                        }
                    });
            captureChannel.on("reconnecting", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("reconnecting " + envelope.getReason() + " " + envelope.getPayload());

                }
            });
            captureChannel.on("connected", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("connected: " + envelope.toString());
                }
            });
            captureChannel.on("_disconnect", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    String code = envelope.getPayload().get("code").asText();
                    if (code.equals("SESSION_OVERRIDE")) {
                        apiCallBack.intermediateCallBack();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


  


    private void parseThemeConfig(JSONObject themeObject) {
        if (themeObject != null) {
           String headerBackgroundColor = themeObject.optJSONObject("custom_header")
                    .optString("backgroundColor");
           themeConfig.setHeaderBackgroundColor(headerBackgroundColor);
            String headerTextColor = themeObject.optJSONObject("custom_header")
                    .optString("color");
            themeConfig.setHeaderTextColor(headerTextColor);
            JSONObject footerObject = themeObject.optJSONObject("custom_footer");
            boolean isDisplayFooter = false;
            if (footerObject != null)
                isDisplayFooter = footerObject.optBoolean("display");
            themeConfig.setDisplayFooter(isDisplayFooter);
            JSONObject paletteObject = themeObject.optJSONObject("palette");
            if (paletteObject != null) {
                JSONObject primaryObject = paletteObject.optJSONObject("secondary");
                if (primaryObject != null) {
                  String  secondaryMainColor = primaryObject.optString("main");
                  themeConfig.setSecondaryMainColor(secondaryMainColor);
                    String secondaryContrastColor = primaryObject.optString("contrastText");
                    themeConfig.setSecondaryContrastColor(secondaryContrastColor);
                }
                JSONObject primaryPaletteObject = paletteObject.optJSONObject("primary");
                if (primaryPaletteObject != null) {
                    String primaryMainColor = primaryPaletteObject.optString("main");
                    themeConfig.setPrimaryMainColor(primaryMainColor);
                   String primaryContrastColor = primaryPaletteObject.optString("contrastText");
                    themeConfig.setPrimaryContrastColor(primaryContrastColor);
                }

            }
        }
    }

    /**
     * A simple {@link ThemeConfig} util class.
     * Use this Theme config for setting the UI for the application
     * @return themeConfig Returns the Theme config object which contains the required details for setting
     * the application theme
     */
    public ThemeConfig getCaptureTheme(){
        return themeConfig;
    }

    /**
     * This method will return the capture Id
     * @return capture id as String
     *
     */
    public String getCaptureId(){
        return captureId;
    }

    /**
     * This method will return the session Id
     * @return session id as String
     *
     */
    public String getSessionId(){
        return sessionId;
    }

    private void setToken(String token){
        this.token = token;
    }

    /**
     * This method will return the Token
     * @return Token as String
     *
     */
    public String getToken(){
        return token;
    }

    /**
     * This method will return the Status of the Capture
     * @return Status as String eg.. Capture_Pending, Review_Required etc
     *
     */
    public String getStatus(){
        return status;
    }

    /**
     * This method will be useed to set your current latitude
     * @param latitude
     */
    public void setLatitude(String latitude){
        this.latitude = latitude;
    }

    /**
     * This method will be used to set your current longitude
     * @param longitude
     */
    public void setLongitude(String longitude){
        this.longitude = longitude;
    }

    /**
     * This method will be useed to set your location accuracy
     * @param accuracy
     */
    public void setAccuracy(String accuracy){
        this.accuracy = accuracy;
    }

    /**
     * This method will be used to set your location altitude
     * @param altitude
     */
    public void setAltitude(String altitude){
        this.altitude = altitude;
    }

    /**
     * This method will be used to set your location speed
     * @param speed
     */
    public void setSpeed(String speed){
        this.speed = speed;
    }

    /**
     * This method will return the Current Latitude
     * @return latitude;
     */
    public String getLatitude(){
        return latitude;
    }

    /**
     * This method will return the Current Longitude
     * @return longitude
     */
    public String getLongitude(){
        return longitude;
    }

    /**
     * This method will return the Location Accuracy
     * @return accuracy
     */
    public String getAccuracy(){
        return accuracy;
    }

    /**
     * This method will return the Location Altitude
     * @return altitude
     */
    public String getAltitude(){
        return altitude;
    }

    /**
     * This method will return the Location Speed
     * @return speed
     */
    public String getSpeed(){
        return speed;
    }

    /**
     * This method will return the FirstPage from the Page Sequence
     * @return listPageSeq
     */
    public PageSequence getFirstPage(){
        return listPageSeq.get(0);
    }

    /**
     * This method will return the CurrentPage from the Page Sequence
     * @return listPageSeq
     */
    public PageSequence getCurrentPage(){
        return listPageSeq.get(pageIndex);
    }

    /**
     * This method will return the Next Page from Page Sequence
     * @return listPageSeq
     */
    public PageSequence onNextPage(){
        pageIndex++;
        if (pageIndex < listPageSeq.size()) {
            return listPageSeq.get(pageIndex);
        }
        return new PageSequence("PAGE_END",null);
    }

    /**
     * This method will set the Request ID For Further Use
     * @param requestId
     */
    public void setRequestId(String requestId){
        this.requestId = requestId;
    }

    /**
     * This method will return the Request ID For Further Use
     * @return requestId
     */
    public String getRequestId(){
        return requestId;
    }

    public void setCalCompleted(boolean isCallCompleted){
        this.isCallCompleted = isCallCompleted;
    }

    public boolean isCallCompleted(){
        return isCallCompleted;
    }

    public void setVideoOverlayEnabled(boolean isVideoOverlayEnabled){
        this.isVideoOverlayEnabled = isVideoOverlayEnabled;
    }

    public boolean isVideoOverlayEnabled(){
        return isVideoOverlayEnabled;
    }

    /**
     * This method is use to get the Session Token. Call this Method when isNetworkCheckNeeded and isRoomJoinNeeded both are false
     * in VideoComponent
     * Pass the below required params
     * @param context Context
     * @param requestId Request ID To get the requestId call {@link #getRequestId()}
     * @param token Token to get the token call {@link #getToken()}
     * @param apiCallBack ApiCallBack which provides the call back of api in
     * onSessionSuccess(), intermediateCallBack(), onSessionFailure().
     */
    public void getSessionToken(Context context, String requestId, String token, ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class).getSessionToken(requestId, token)
                .enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        try {
                            if (response.isSuccessful()) {
                                JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                                apiCallBack.onSessionSuccess(jsonObject);
                                //  successCallBack.onTokenReceived(sessionToken);

                            } else {
                                try {
                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                    Log.d("==SESSION",jObjError.toString());
                                    apiCallBack.onSessionFailure(jObjError.getString("reason"));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            apiCallBack.onSessionFailure(response.errorBody().toString());
                        }

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Log.d("==ERROR", t.getMessage());
                        apiCallBack.onSessionFailure(t.getMessage());

                    }
                });

    }

    /**
     * In Page assisted-vkyc you will receive two boolean variable isNetworkCheckNeeded and isRoomJoinNeeded if both are true
     * you have to call this method to initiate health check
     * In Success callback you will get network_check_id, participant_id, room_id
     * @param context Context
     * @param token TOKEN
     * @param object JsonObject {"reference_id":"{@link #getRequestId()}","config":{"room_join":isRoomJoinNeeded,"timeout":10}}
     * @param apiCallBack ApiCallBack which provides the call back of api in
     * onSessionSuccess(), intermediateCallBack(), onSessionFailure().
     */
    public void initHealthCheck(Context context, String token, JsonObject object,
                                ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class)
                .initHealthCheck(token, object).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    //JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("===RES", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        apiCallBack.onSessionSuccess(jsonObject);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("==ERROR", String.valueOf(response.errorBody().toString()));
                    apiCallBack.onSessionFailure(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiCallBack.onSessionFailure(t.getMessage());
            }
        });

    }

    /**
     * This method is used to Submit your network health check after you are done with {@link #initHealthCheck}
     * @param context Context
     * @param token TOKEN
     * @param object JsonObject
     * @param apiCallBack ApiCallBack which provides the call back of api in
     * onSessionSuccess(), intermediateCallBack(), onSessionFailure().
     */
    public void submitHealthCheck(Context context, String token, JsonObject object,
                                  ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class)
                .submitHealthCheck(token, object).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    //JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("===RES", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        apiCallBack.onSessionSuccess(jsonObject);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("==ERROR", String.valueOf(response.errorBody().toString()));
                    apiCallBack.onSessionFailure(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiCallBack.onSessionFailure(t.getMessage());
            }
        });

    }
    /**
     * This method is use to Upload any Image to the server with the following params
     * @param context Context
     * @param url URL to upload the file
     * @param path local path of the file where it locates
     * @param apiCallBack ApiCallBack which provides the call back of api in
     * onSessionSuccess(), intermediateCallBack(), onSessionFailure().
     */
    public void uploadImage(Context context, String url, String path, ApiCallBack apiCallBack){
        File file = new File(path);
        InputStream in = null;
        RequestBody requestFile = null;
        try {
            in = new FileInputStream(file);
            byte[] buf;
            buf = new byte[in.available()];
            while (in.read(buf) != -1) ;
            requestFile = RequestBody.create(null, buf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class).uploadScreenShot(url,requestFile)
                .enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()){
                            apiCallBack.onSessionSuccess(null);
                        }else {
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                apiCallBack.onSessionFailure(jObjError.getString("message"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        apiCallBack.onSessionFailure(t.getMessage());
                    }
                });
    }


    public void log(Context context, JSONObject object, String token, String loggerUrl, ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class)
                .logger(loggerUrl, token, object).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Log.d("===Logger RES", response.body().toString());
                } else {
                    Log.d("==Logger ERROR", String.valueOf(response.errorBody().toString()));
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("==Logger Failure", t.getMessage());
                apiCallBack.onSessionFailure(t.getMessage());
            }
        });

    }

    public void logWebHook(Context context, Map<String, Object> object, String loggerUrl, ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class)
                .logger(loggerUrl, object).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Log.d("===Logger RES", response.body().toString());
                } else {
                    Log.d("==Logger ERROR", String.valueOf(response.errorBody().toString()));
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("==Logger Failure", t.getMessage());
                apiCallBack.onSessionFailure(t.getMessage());
            }
        });

    }

    public String getTatSince(long start) {
        long tat = Math.round(System.currentTimeMillis() - start);
        return String.valueOf(tat);
    }

}
