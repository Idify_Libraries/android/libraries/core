package com.idfy.core;

import androidx.fragment.app.Fragment;

public abstract class BackPressEvent extends Fragment {
    public abstract boolean onBackPress();
}
